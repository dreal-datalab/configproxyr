
<!-- README.md is generated from README.Rmd. Please edit that file -->

# configproxyr

<!-- badges: start -->
<!-- badges: end -->

Le but de `{configproxyr}` est d’automatiser le passage d’un
environnement en proxy d’un environnement hors proxy

## Installation

``` r
remotes::install_gitlab("dreal-datalab/configproxyr")
```

## Usage

`{configproxyr}` contient les fonctions suivantes :

-   `check_proxy()` vous informe de la configuration de votre proxy dans
    les fichiers `.Renviron`, `.gitconfig` et `.ssh/config`.

-   `activate_proxy()` active le proxy dans les fichiers `.Renviron`,
    `.gitconfig` et `.ssh/config`.

-   `deactivate_proxy()` désactive le proxy dans les fichiers
    `.Renviron`, `.gitconfig` et `.ssh/config`.

-   `explain_proxy()` explique les étapes de configuration du proxy
